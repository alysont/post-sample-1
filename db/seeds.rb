# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
(1..100).each do |n|
  Author.create(
    name: Faker::Name.name_with_middle,
    email: Faker::Internet::email,
    company: Faker::Company.name
    )
end

(1..1000).each do |n|
  author_id = Author.pluck(:id).sample
  Post.create(
    title: Faker::Hacker.adjective,
    description: Faker::Hacker.say_something_smart,
    author: Author.find(author_id)
    )
end